#!/bin/bash
#
# This script run all the extractors chained together, 
# the result is an XML sent to the stdout.
#
# iskoda@fit.vutbr.cz
source rrs_local/profile.sh

cd extractors # Extractors must be executed in extractors/ directory because paths to python executables are releative to this directory.
./extractPlaintext.py somefile.pdf < ../articles/somefile.pdf | ./wrapDocument.py | ./languageDetector.py | ./findKeywords.py | ./findAbstract.py | ./getTitle.py | ./getAuthorsAndEmails.py | ./getPublisher.py | ./getChapters.py | ./getCitations.py | ./getXml.py

# This is the default order of extractors
# The order of extractPlaintext.py and wrapDocument.py is mandatory - they must be called as first in given order.
# getXml.py must be called as the last - it gives you output XML.
#./extractPlaintext.py somefile.pdf < somefile.pdf | ./wrapDocument.py | ./languageDetector.py | ./findKeywords.py | ./findAbstract.py | ./getTitle.py | ./getAuthorsAndEmails.py | ./getPublisher.py | ./getChapters.py | ./getCitations.py | ./getXml.py



