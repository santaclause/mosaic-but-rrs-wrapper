#!../rrs_local/bin/python -u

"""
#
# Extract plaintext
#
# Must be called exactly from extractions/ directory in rrs-extractions package because of
# relative path to python binary.
#
# Takes a pdf filename as first argument and pdf file content to stdin.
# Returns JSON object to stdout.
# Example: ./extractPlaintext.py somefilename.pdf < /tmp/somepdffile.pdf
#
"""


import sys
import os
import json
import tempfile
import subprocess
import pickle
from encodings.base64_codec import base64_encode

from rrslib.classifiers.documentinfo import DocumentInfo, MISC, INPROCEEDINGS
from rrslib.db.model import RRSModule, RRSFile, RRSRelationshipFileUrl, RRSUrl, RRSText
from rrslib.extractors.normalize import TextCleaner
from rrslib.extractors.articlemetaextractor import *
#from rrslib.extractors.articlemetaextractor import ArticleMetaExtractor
#from rrslib.extractors.normalize import TextCleaner
#from rrslib.xml.xmlconverter import Model2XMLConverter

#import shutil

def main():
    
    # Descriptions of text extractor scripts status codes
    statusCodes = {
        0: 'OK - text is properly extracted.',
        1: 'SCAN - The PDF was probably scanned - too small amount of text per page.',
        2: 'BAD - the quality of extraction is bad.',
        3: 'MANUAL - cannot resolve quality of extraction, it should be checked manually.',
        4: 'ERROR - Extraction was unsuccessfull, no output has been generated.',
    }
    
    pdfFileName = sys.argv[1]
    
    pdf = sys.stdin.read()
    
    pdfTmpFile = tempfile.NamedTemporaryFile(mode='w+b', suffix='.pdf')
    pdfTmpFile.file.write(pdf);
    pdfTmpFile.file.flush();
    
    #print pdfTmpFile.name
    
    # Extract text from PDF and get the text with statuscode of text extractor
    text, textFilePath, statusCode = extractText(pdfTmpFile.name)
    
    # Prepare RRS files info
    files = [textFilePath, pdfTmpFile.name] # The order must be - TXT file, PDF file 
    rrsFiles = []
    for file in files:
        rrsFile = RRSFile()
        _rel = RRSRelationshipFileUrl()
        _rel.set_entity(RRSUrl(link=pdfFileName.replace('.pdf', '.txt')))
        rrsFile.set('url', _rel)
        #fname = self._input_file.split('/')
        #fname.reverse()
        rrsFile.set('filename', pdfFileName.replace('.pdf', '.txt'))
        rrsFile.set('size', int(len(text)))
        rrsFile.set('type', str('txt'))
        rrsFiles.append(rrsFile)
    
    # Clean text
    text = unicode(text, 'utf-8', errors='ignore') # Unicode characters are left behind
    text = TextCleaner().clean_text(text)
    
    # Document info
    documentInfo = DocumentInfo()
    pub_type = MISC
    articles = documentInfo.get_articles(text) 
    if len(articles) > 1:
        pub_type = INPROCEEDINGS
    else:
        articles = [text]
        
    # Create publications with basic data for each article 
    rrsTexts = [] 
    publications = []
    for text in articles:
        # New publication - for each rrsText
        publication = RRSPublication()
        # RRSText
        rrsText = RRSText(content=text, length=len(text))
        rrsText.set('file', rrsFiles[0])
        rrsTexts.append(rrsText)
        publication.set('text', rrsText)
        # Store module information into publication
        publication.set('module', 'publication_text_data')
        # Magic with original files
        # Get files and store them into publication
        txt_file_path = textFilePath
        pdf_file_path = pdfTmpFile.name
        for file in rrsFiles:
            _rel = RRSRelationshipFilePublication()
            _rel.set_entity(file)
            publication.set('file', _rel)
        publication.set('type', RRSPublication_type(type=pub_type))
        
        
        publications.append(publication)

    # Prepare JSON with text and status code
    data = {
        'text': text,
        'textStatusCode': statusCode,
        'textStatusCodeDescription': statusCodes[statusCode],
        'pubType': pub_type,
        'articlesSerialized': pickle.dumps(articles),
        #'rrsFilesSerialized': pickle.dumps(rrsFiles),
        'publicationsSerialized': pickle.dumps(publications),
        #'rrsTextsSerialized': pickle.dumps(rrsTexts)
    }
    
    
    # Clean temp files
    os.remove(textFilePath)
    pdfTmpFile.close()
    
    # Data in JSON to stdout
    json.dump(data, sys.stdout)
    
    


def extractText(pdfFilePath):
    """
    Extracts the text from PDF file and checks quality of extraction.
    @param pdfFilePath:            string Path of the PDF file 
    @return: tuple(string, int)    string - text of the PDF; int - statuscode of extractor 
    """
    # Path to text extractor
    textExtractorPath = "../to_text/prevodPdf.sh"
    
    #subprocess.call(['ls', '-a'], stdout=sys.stdout, bufsize=0)
    
    exitCode = subprocess.call([textExtractorPath, pdfFilePath])
    
    # Load the extracted text file
    if os.path.isfile(pdfFilePath):
        textFilePath = pdfFilePath.replace('.pdf', '.txt')
        textFile = open(textFilePath, 'r')
        text = textFile.read()
        textFile.close()
    else:
        text = ''
    
    return text, textFilePath, exitCode
    
    
     
 

if __name__ == "__main__":
    main()

