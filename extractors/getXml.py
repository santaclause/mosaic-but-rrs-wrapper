#!../rrs_local/bin/python

#
# Get metadata XML
# Writes XML to stdout
#

import sys
#import os
import json
#import tempfile
#import subprocess
import pickle
from rrslib.xml.xmlconverter import *

def main():
    jsonIn = sys.stdin.read()
    data = json.loads(jsonIn)

    # Get XML of metadata
    publications = pickle.loads(data['publicationsSerialized'].encode('ascii'))
    converter = Model2XMLConverter(stream=sys.stdout)
    converter.convert(publications)
    

if __name__ == "__main__":
    main()
