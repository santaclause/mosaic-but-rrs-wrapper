#!../rrs_local/bin/python

#
# Get chapters
#

import sys
#import os
import json
#import tempfile
#import subprocess
import pickle
from rrslib.extractors.articlemetaextractor import *
from rrslib.extractors.citationentityextractor import *
from rrslib.extractors.entityextractor import *

def main():
    jsonIn = sys.stdin.read()
    data = json.loads(jsonIn)

    # For each article get title
    textualDocuments = pickle.loads(data['textualDocuments'].encode('ascii'))
    publications = pickle.loads(data['publicationsSerialized'].encode('ascii'))
    for i, textualDocument in enumerate(textualDocuments):
        # Get citations from document and store them into publication
        for cit in textualDocument.get_citations():
            if cit == None:
                continue
            _cit = CitationEntityExtractor(ALL).extract(cit)
            if _cit.isset('reference'):
                _cit['reference']['publication'] = publications[i]
            _rel = RRSRelationshipPublicationCitation()
            _rel.set_entity(_cit)
            publications[i].set("citation", _rel)

    
    # Save publications back to data object
    data['publicationsSerialized'] = pickle.dumps(publications)
    
    
    # Data in JSON to stdout
    json.dump(data, sys.stdout)


if __name__ == "__main__":
    main()
