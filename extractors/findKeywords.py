#!../rrs_local/bin/python

#
# Find keywords
# Probably modifies textsWithMeta
#

import sys
#import os
import json
#import tempfile
#import subprocess
import pickle
from rrslib.extractors.articlemetaextractor import *

def main():
    jsonIn = sys.stdin.read()
    data = json.loads(jsonIn)

    # For each article detect keywords
    textsWithMeta = pickle.loads(data['textsWithMeta'].encode('ascii'))
    publications = pickle.loads(data['publicationsSerialized'].encode('ascii'))
    for i, textWithMeta in enumerate(textsWithMeta):
        # Get keywords and store them into publication
        keywords = MetaExtractor().find_keywords(textWithMeta)
        for keyword in keywords[0]:
            _rel = RRSRelationshipPublicationKeyword()
            _rel.set_entity(keyword)
            publications[i].set('keyword', _rel)
            textsWithMeta[i] = keywords[1]
    
    # Save publications back to data object
    data['publicationsSerialized'] = pickle.dumps(publications)
    data['textsWithMeta'] = pickle.dumps(textsWithMeta)
    
    
    # Data in JSON to stdout
    json.dump(data, sys.stdout)


if __name__ == "__main__":
    main()
