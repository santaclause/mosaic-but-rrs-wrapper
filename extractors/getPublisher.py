#!../rrs_local/bin/python

#
# Get publisher
#

import sys
#import os
import json
#import tempfile
#import subprocess
import pickle
from rrslib.extractors.articlemetaextractor import *
from rrslib.extractors.entityextractor import *

def main():
    jsonIn = sys.stdin.read()
    data = json.loads(jsonIn)

    # For each article get title
    textsWithMeta = pickle.loads(data['textsWithMeta'].encode('ascii'))
    publications = pickle.loads(data['publicationsSerialized'].encode('ascii'))
    for i, textWithMeta in enumerate(textsWithMeta):
        # Get publisher from document and store it into publication
        publisher = EntityExtractor().find_publisher(textWithMeta)
        publications[i].set('publisher', RRSOrganization(title=publisher[0]))
        textsWithMeta[i] = publisher[1]

    
    # Save publications back to data object
    data['publicationsSerialized'] = pickle.dumps(publications)
    data['textsWithMeta'] = pickle.dumps(textsWithMeta)
    
    
    # Data in JSON to stdout
    json.dump(data, sys.stdout)


if __name__ == "__main__":
    main()
