#!../rrs_local/bin/python

#
# Language detector
#
#

import sys
#import os
import json
#import tempfile
#import subprocess
import pickle
from rrslib.extractors.articlemetaextractor import *

def main():
    jsonIn = sys.stdin.read()
    data = json.loads(jsonIn)

    # For each article detect language
    textsWithMeta = pickle.loads(data['textsWithMeta'].encode('ascii'))
    publications = pickle.loads(data['publicationsSerialized'].encode('ascii'))
    for i, textWithMeta in enumerate(textsWithMeta):
        # Get and store publication language
        lang_data = LanguageIdentifier().identify(textWithMeta)
        lang = RRSLanguage(name=lang_data[0])
        cred = int(lang_data[1] * 2)
        if cred > 100: cred = 100
        lang.set('credibility', cred)
        publications[i].set('language', lang)
    
    # Save publications back to data object
    data['publicationsSerialized'] = pickle.dumps(publications)
    
    
    # Data in JSON to stdout
    json.dump(data, sys.stdout)


if __name__ == "__main__":
    main()
