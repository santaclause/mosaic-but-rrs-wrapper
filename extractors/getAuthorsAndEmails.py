#!../rrs_local/bin/python

#
# Find Emails and Authors and assign them together
# Probably modifies textsWithMeta
# Maybe should not be called before getTitle.py
#

import sys
#import os
import json
#import tempfile
#import subprocess
import pickle
from rrslib.extractors.articlemetaextractor import *
from rrslib.extractors.entityextractor import *

def main():
    jsonIn = sys.stdin.read()
    data = json.loads(jsonIn)

    # For each article
    textsWithMeta = pickle.loads(data['textsWithMeta'].encode('ascii'))
    publications = pickle.loads(data['publicationsSerialized'].encode('ascii'))
    for i, textWithMeta in enumerate(textsWithMeta):
        # Get emails
        emailExtractor = EntityExtractor().EmailExtractor()
        emails = emailExtractor.get_emails(textsWithMeta[i])
        textsWithMeta[i] = emailExtractor.get_rest()

        # Get names and assign emails and store them into publication
        names = EntityExtractor().find_authors(textsWithMeta[i])
        assigned_names = ArticleMetaExtractor()._assign_emails(emails, names[0])
        c = 0
        for name in assigned_names:
            c += 1
            _rel = RRSRelationshipPersonPublication(author_rank=c, editor=False)
            _rel.set_entity(name)
            publications[i].set('person', _rel)
        textsWithMeta[i] = names[1]


    
    # Save publications back to data object
    data['publicationsSerialized'] = pickle.dumps(publications)
    data['textsWithMeta'] = pickle.dumps(textsWithMeta)
    
    
    # Data in JSON to stdout
    json.dump(data, sys.stdout)


if __name__ == "__main__":
    main()
