#!../rrs_local/bin/python

#
# Get chapters
#

import sys
#import os
import json
#import tempfile
#import subprocess
import pickle
from rrslib.extractors.articlemetaextractor import *
#from rrslib.extractors.entityextractor import *

def main():
    jsonIn = sys.stdin.read()
    data = json.loads(jsonIn)

    # For each article get title
    textualDocuments = pickle.loads(data['textualDocuments'].encode('ascii'))
    publications = pickle.loads(data['publicationsSerialized'].encode('ascii'))
    for i, textualDocument in enumerate(textualDocuments):
        # Get chapters from document and store them into publication
        for chpt in textualDocument.get_chapters():
            _rel = RRSRelationshipPublication_sectionPublication()
            _rel.set_entity(chpt)
            publications[i].set("publication_section", _rel)

    
    # Save publications back to data object
    data['publicationsSerialized'] = pickle.dumps(publications)
    
    
    # Data in JSON to stdout
    json.dump(data, sys.stdout)


if __name__ == "__main__":
    main()
