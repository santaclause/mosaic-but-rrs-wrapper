#!../rrs_local/bin/python

#
# Find abstract
# Probably modifies textsWithMeta
# Maybe should not be called before findKeywords.py
#

import sys
#import os
import json
#import tempfile
#import subprocess
import pickle
from rrslib.extractors.articlemetaextractor import *

def main():
    jsonIn = sys.stdin.read()
    data = json.loads(jsonIn)

    # For each article find abstract
    textsWithMeta = pickle.loads(data['textsWithMeta'].encode('ascii'))
    publications = pickle.loads(data['publicationsSerialized'].encode('ascii'))
    for i, textWithMeta in enumerate(textsWithMeta):
        # Get abstract and store it into publication
        abstract = MetaExtractor().find_abstract(textWithMeta)
        publications[i].set('abstract', abstract[0])
        textsWithMeta[i] = abstract[1]
    
    # Save publications back to data object
    data['publicationsSerialized'] = pickle.dumps(publications)
    data['textsWithMeta'] = pickle.dumps(textsWithMeta)
    
    
    # Data in JSON to stdout
    json.dump(data, sys.stdout)


if __name__ == "__main__":
    main()
