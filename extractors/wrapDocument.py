#!../rrs_local/bin/python

#
# Document wrapper
#
#

import sys
#import os
import json
#import tempfile
#import subprocess
import pickle
#from encodings.base64_codec import base64_encode
from rrslib.extractors.documentwrapper import *

def main():
    jsonIn = sys.stdin.read()
    data = json.loads(jsonIn)

    # For each article text create TextualDocument and textsWithMeta
    articles = pickle.loads(data['articlesSerialized'].encode('ascii'))
    documentWrapper = DocumentWrapper()
    textualDocuments = []
    textsWithMeta = []
    for text in articles:
        # Wrap document
        textualDocument = documentWrapper.wrap(text)
        textualDocuments.append(textualDocument)
        textsWithMeta.append(textualDocument.get_meta())
    
    # Add TextualDocument and textsWithMeta to data
    data['textualDocuments'] = pickle.dumps(textualDocuments)
    data['textsWithMeta'] = pickle.dumps(textsWithMeta)
    
    
    # Data in JSON to stdout
    json.dump(data, sys.stdout)


if __name__ == "__main__":
    main()
