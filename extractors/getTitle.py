#!../rrs_local/bin/python

#
# Get title
# Probably modifies textsWithMeta
# Maybe should not be called before findAbstract.py
#

import sys
#import os
import json
#import tempfile
#import subprocess
import pickle
from rrslib.extractors.articlemetaextractor import *

def main():
    jsonIn = sys.stdin.read()
    data = json.loads(jsonIn)

    # For each article get title
    textsWithMeta = pickle.loads(data['textsWithMeta'].encode('ascii'))
    publications = pickle.loads(data['publicationsSerialized'].encode('ascii'))
    for i, textWithMeta in enumerate(textsWithMeta):
        # Get title from document and store it into publication
        title = MetaExtractor().find_title(textWithMeta)
        publications[i].set('title', title[0])
        textsWithMeta[i] = title[1]

    
    # Save publications back to data object
    data['publicationsSerialized'] = pickle.dumps(publications)
    data['textsWithMeta'] = pickle.dumps(textsWithMeta)
    
    
    # Data in JSON to stdout
    json.dump(data, sys.stdout)


if __name__ == "__main__":
    main()
