#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This extract metadata from text files using rrs_lib ArticleMetaExtractor.

Following programs and libraries are also needed:    
    rrs_library
"""


__modulename__ = "publicationtextdata"
__author__ = "Tomas Lokaj"
__email__ = "xlokaj03@stud.fit.vutbr.cz"
__date__ = "$20-October-2011"


################################################################################
#Constants
################################################################################
H_USAGE = \
"""
---------------------------------------------------------------------------------------
| Usage: -i input_text_file -o output_xml_file [-t time_limit] [-p original_file]     |
| NOTE:                                                                               |
|       default time limit is 1500s                                                   | 
|       original file is absolute path to the original of the text_file (pdf, ps,...) |                                                |
---------------------------------------------------------------------------------------
"""
################################################################################
#End of constants
################################################################################


################################################################################
#Imports
################################################################################
from rrslib.classifiers.documentinfo import DocumentInfo, MISC, INPROCEEDINGS
from rrslib.db.model import RRSModule, RRSFile, RRSRelationshipFileUrl, RRSUrl
from rrslib.extractors.articlemetaextractor import ArticleMetaExtractor
from rrslib.extractors.normalize import TextCleaner
from rrslib.xml.xmlconverter import Model2XMLConverter
import StringIO
import getopt
import os
import signal
import sys
import time

try:
    import psyco
    psyco.full()
except ImportError:
    pass
except:
    sys.stderr.write("An error occured while starting psyco.full()")
################################################################################
#End of imports
################################################################################


################################################################################
#Timer
################################################################################
def handler(signum, frame): 
    raise Exception("Timeout")
signal.signal(signal.SIGALRM, handler)
################################################################################
#End of Timer
################################################################################


################################################################################
#Class PublicatoinTextData
################################################################################
class PublicationTextData(object):
    """
    This class handles the main parts of metadata extraction from files. 
    """
    
    def __init__(self):
        self._output_file = None
        self._input_file = None
        self._time_limit = 1500
        self._original_file = None
        self._module = RRSModule(module_name='publication_text_data')
        self._document_info = DocumentInfo()
        self._ame = ArticleMetaExtractor()
        self._tfs = TextCleaner()
    
    
    def _make_output_dir(self):
        dir = ""
        f_split = self._output_file.split("/")
        for i in range(0, len(f_split)):
            dir = "/".join(f_split[0:i])
            try:
                os.mkdir(dir)
            except:
                pass
            
    
    def do_params(self, params, optlist):
        """
        Arguments checking.
        """
        if params:
            print H_USAGE
            exit()
        opts = map(lambda x: x[0], optlist)
        if not opts:
            print H_USAGE
            exit()
        if "--help" in opts:
            print H_USAGE
            exit()    
        else:          
            if "-i" in opts and "-o" in opts:
                self._input_file = \
                    filter(lambda x: x[0] == "-i", optlist)[0][1]
                self._output_file = \
                    filter(lambda x: x[0] == "-o", optlist)[0][1]
                if "-t" in opts:
                    try:
                        self._time_limit = \
                            int(filter(lambda x: x[0] == "-t", optlist)[0][1])
                        if self._time_limit <= 0:
                            self._time_limit = 300
                    except:
                        pass
                if "-p" in opts:
                    self._original_file = \
                        filter(lambda x: x[0] == "-p", optlist)[0][1]
            else:
                print H_USAGE
                exit()
        
        
    def extract_metadata(self, input_file=None, output_file=None,
                         time_limit=None, original_file=None):
        """
        Metadata extraction.
        
        @param input_file: input text file
        @type input_file: str
        @param output_file: output xml file
        @type output_file: str
        @param time_limit: time_limit per one article in input_file
        @type input_file: int
        @param original_file: absolute path to original file (pdf, ps, ...)
        @type input_file: str
        """
        if input_file != None:
            self._input_file = input_file
        if output_file != None:
            self._output_file = output_file
        if time_limit != None:
            self._time_limit = int(time_limit)
        if original_file != None:
            self._original_file = int(original_file)
            
        if self._input_file==None or self._output_file==None:
            sys.stderr.write("Input and source file must be specified! input:"+str(input_file)+" output:"+str(output_file)+"\n")
            exit()
    
        res = False
        
        self._make_output_dir()
    
        tm1 = time.time()
        print "Extracting metadata...",
        sys.stdout.flush()
        
        signal.alarm(self._time_limit)
        try:
            text = self._tfs.read(self._input_file, clean=True)
            files_info = []
    
            if self._original_file != None:
                files = [self._original_file,self._input_file]
            else:
                files=[self._input_file]
            for f in files:
                try: 
                    rrsf = RRSFile()
                    _rel = RRSRelationshipFileUrl()
                    _rel.set_entity(RRSUrl(link=str(f)))
                    rrsf.set('url', _rel)
                    fname = self._input_file.split('/')
                    fname.reverse()
                    rrsf.set('filename', str(os.path.split(f)[1]))
                    rrsf.set('size', int(os.path.getsize(f)))
                    rrsf.set('type', str(os.path.split(f)[1]).split(".")[1])
                    files_info.append(rrsf)
                except OSError:
                    continue
            
            pub_type = MISC
            articles = self._document_info.get_articles(text) 
            if len(articles) > 1:
                pub_type = INPROCEEDINGS
            else:
                articles = [text]
                
            converted_output = StringIO.StringIO()
            converter = Model2XMLConverter(stream=converted_output)
            publications = []
            for text in articles:
                signal.alarm(self._time_limit)
                publication = self._ame.extract_data(text,
                                               self._module.get('module_name'),
                                               files_info, pub_type)
                publications.append(publication)
            converter.convert(publications)
            of = open(self._output_file, 'w')
            of.write(converted_output.getvalue())
            of.flush()
            of.close()
                
            signal.alarm(0)
            res = True
            print "\tOK"
        except:
            print "\tERROR"
        finally:
            signal.alarm(0)
        
        tm2 = time.time()
        print "Done in " + str(tm2 - tm1) + " seconds"
        return res
            
            
    def start(self):
        """
        Main method.
        """
        return self.extract_metadata()
################################################################################
#End of class PublicatonTextData
################################################################################

if __name__ == '__main__':
    PTD = PublicationTextData()
    try:
        optlist, params = getopt.getopt(sys.argv[1:], 'o:i:l:', ['help'])
        PTD.do_params(params, optlist)
    except getopt.GetoptError:
        print H_USAGE
        exit()
    PTD.start()
